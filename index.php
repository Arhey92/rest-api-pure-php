<?php
/**
 * Created by PhpStorm.
 * User: Arhey
 * Date: 15.04.2019
 * Time: 21:43
 */

$method = $_SERVER['REQUEST_METHOD'];

include_once $_SERVER['DOCUMENT_ROOT'].'\app\helpers\filter.php';

$formData = getFormData($method);
$class = ucwords($formData['class']);
if(isset($formData['child_class'])){
    $class = ucwords($formData['child_class']);
}

include_once $_SERVER['DOCUMENT_ROOT'].'\app\controllers\\'.$class.'Controller.php';

unset($formData['class']);

$reflection = new ReflectionClass('\app\controllers\\'.$class.'Controller');
$instance = $reflection->newInstanceArgs(array($method, $formData));

return $instance;