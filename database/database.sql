DROP TABLE IF EXISTS `doit`.`users`;
CREATE TABLE  `doit`.`users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `doit`.`user_tokens`;
CREATE TABLE  `doit`.`user_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) NOT NULL,
  `token` varchar(190) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `doit`.`user_tasks`;
CREATE TABLE  `doit`.`user_tasks` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`user_id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `due_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`priority` ENUM('Low', 'Normal', 'Hight'),
	`status` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;