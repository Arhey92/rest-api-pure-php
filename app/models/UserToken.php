<?php

namespace app\models;

require_once 'config/connect.php';

use config\DbConnect;

class UserToken
{
    private $table = 'user_tokens';
    private $db;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->db = DbConnect::getInstance();
    }

    /**
     * @param $userId
     * @param $jwt
     * @return array
     */
    public function create($userId, $jwt)
    {
        //store token to table
        $sql = 'INSERT '.$this->table.' (user_id, token) VALUES (:user_id,:token)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':user_id', $userId);
        $stmt->bindValue(':token', $jwt);
        $stmt->execute();

        if($stmt->rowCount() > 0)
        {
            return array('status' => 201, 'token' => $jwt);
        }else{
            return [];
        }
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getTokenByUserId($userId)
    {
        $sql = 'SELECT token FROM '.$this->table.' WHERE user_id = :user_id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $userId);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $userToken = $stmt->fetchObject();

            return $userToken->token;
        }

        die();
    }
}