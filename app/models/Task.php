<?php

namespace app\models;

require_once 'config/connect.php';

use config\DbConnect;
use PDO;

class Task
{
    private $table = 'user_tasks';
    private $db;

    private $limit;
    private $page;
    private $total;
    private $query;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->db = DbConnect::getInstance();
    }

    /**
     * @param int $userId
     * @param $sort
     * @param int $limit
     * @param int $page
     * @return array
     */
    public function getList(int $userId, $sort, $limit = 2, $page = 1)
    {
        $this->limit = $limit;
        $this->page = $page;

        $this->query = 'SELECT * FROM '.$this->table.' WHERE user_id = :user_id ORDER BY priority '.$sort;

        if ( $this->limit == 'all' ) {
            $query = $this->query;
        } else {
            $query = $this->query . " LIMIT " . (( $this->page - 1 ) * $this->limit ) . ", $this->limit";
        }

        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':user_id', $userId);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $count_query = 'SELECT * FROM '.$this->table.' WHERE user_id = :user_id';
            $query = $this->db->prepare($count_query);
            $query->bindValue(':user_id', $userId);
            $query->execute();
            $count = $query->rowCount();

            //calculate the pagination number by dividing total number of rows with per page.
            $this->total = ceil($count / $limit);

            $results = $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $result = [];
            $result['page'] = $this->page;
            $result['limit'] = $this->limit;
            $result['total'] = $this->total;
            $result['tasks'] = $results;

            return $result;
        }else{
            return array('tasks' => []);
        }
    }

    /**
     * @param int $userId
     * @param array $data
     * @return array
     */
    public function create(int $userId, array $data)
    {
        $sql = 'INSERT '.$this->table.' (user_id, title, due_date, priority) VALUES (:user_id, :title, :due_date, :priority)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':user_id', $userId);
        $stmt->bindValue(':title', $data['title']);
        $stmt->bindValue(':due_date', $data['due_date']);
        $stmt->bindValue(':priority', $data['priority']);
        $stmt->execute();


        if($stmt->rowCount() > 0)
        {
            return array('status' => 201);
        }else{
            return [];
        }
    }

    /**
     * @param int $taskId
     * @return array
     */
    public function completed(int $taskId)
    {
        $sql = 'UPDATE '.$this->table.' SET status = :status WHERE id = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':status', 1);
        $stmt->bindValue(":id", $taskId);
        $stmt->execute();

        if($stmt->rowCount() > 0)
        {
            return array('status' => 200);
        }else{
            return [];
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroy(int $id)
    {
        $sql = 'DELETE FROM '.$this->table.' WHERE id = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        if($stmt->rowCount() > 0)
        {
            return array('status' => 204);
        }else{
            return [];
        }
    }
}