<?php

namespace app\models;

require_once 'config/connect.php';
require_once 'app/models/UserToken.php';

use config\DbConnect;
use PDO;

class User
{
    private $table = 'users';
    private $db;
    private $userId;

    const SECRET = "DOIT Software";
    const ALGORITHM = "sha256";

    // JWT Header
    private $header = [
        "alg"     => "sha256",
        "typ"     => "JWT"
    ];

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->db = DbConnect::getInstance();
    }

    public function setUserId($id)
    {
        return $this->userId = $id;
    }

    /**
     * @param int $id
     * @return array|mixed
     */
    public function getById(int $id)
    {
        $sql = 'SELECT * FROM '.$this->table.' WHERE id = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }else{
            return [];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        $sql = 'SELECT id FROM '.$this->table.' WHERE email = :email';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":email", $data['email']);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            echo json_encode("Email is already exist in db.");
            die();
        }

        $this->db->beginTransaction();

        try{
            $sql = 'INSERT '.$this->table.' (email, password) VALUES (:email,:password)';
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':email', $data['email']);
            $stmt->bindValue(':password', password_hash($data['password'], PASSWORD_DEFAULT));
            $stmt->execute();

            $userId = $this->db->lastInsertId();

            //create JWT token
            $payload = [
                'user_id' => $userId,
                'email' => $data['email']
            ];
            $jwt = $this->generateJWT(self::ALGORITHM, $this->header, $payload, self::SECRET);

            $userToken = new UserToken();
            $userToken->create($userId, $jwt);
        }catch (\Exception $exception){
            $this->db->rollBack();

            echo $exception->getMessage();
            die();
        }

        $this->db->commit();

        if($stmt->rowCount() > 0)
        {
            return array('status' => 201, 'token' => $jwt);
        }else{
            return [];
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroy(int $id)
    {
        $sql = 'DELETE FROM '.$this->table.' WHERE id = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        if($stmt->rowCount() > 0)
        {
            return array('status' => 204);
        }else{
            return [];
        }
    }

    public function isUserDataCorrect($email, $password)
    {
        $res[] = $this->isUserEmailCorrect($email);
        $res[] = $this->isUserPasswordCorrect($password);

        $returnToken = (in_array(false, $res))? false : true;

        if($returnToken && !is_null($this->userId)){
            $userToken = new UserToken();
            $token = $userToken->getTokenByUserId($this->userId);

            return array('status' => 200, 'token' => $token);
        }

        return false;
    }

    public function isUserEmailCorrect($email)
    {
        $sql = 'SELECT id FROM '.$this->table.' WHERE email = :email';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":email", $email);
        $stmt->execute();

        $this->userId = null;
        if($stmt->rowCount() > 0){
            $user = $stmt->fetchObject();
            $this->setUserId($user->id);

            return true;
        }

        return false;
    }

    public function isUserPasswordCorrect($password)
    {
        if (password_verify($password, password_hash($password, PASSWORD_DEFAULT))) {
            return true;
        }

        return false;
    }

    /**
     * @param string $data
     * @return string
     */
    private function base64UrlEncode(string $data): string
    {
        $urlSafeData = strtr(base64_encode($data), '+/', '-_');

        return rtrim($urlSafeData, '=');
    }

    /**
     * @param string $data
     * @return string
     */
    private function base64UrlDecode(string $data): string
    {
        $urlUnsafeData = strtr($data, '-_', '+/');

        $paddedData = str_pad($urlUnsafeData, strlen($data) % 4, '=', STR_PAD_RIGHT);

        return base64_decode($paddedData);
    }

    /**
     * @param string $algorithm
     * @param array $header
     * @param array $payload
     * @param string $secret
     * @return string
     */
    private function generateJWT(string $algorithm, array $header, array $payload, string $secret): string
    {
        $headerEncoded = $this->base64UrlEncode(json_encode($header));

        $payloadEncoded = $this->base64UrlEncode(json_encode($payload));

        // Delimit with period (.)
        $dataEncoded = "$headerEncoded.$payloadEncoded";

        $rawSignature = hash_hmac($algorithm, $dataEncoded, $secret, true);

        $signatureEncoded = $this->base64UrlEncode($rawSignature);

        // Delimit with second period (.)
        $jwt = "$dataEncoded.$signatureEncoded";

        return $jwt;
    }

    /**
     * @param null $algorithm
     * @param string $jwt
     * @param null $secret
     * @return bool
     */
    function verifyJWT(string $jwt, $algorithm = null, $secret = null): bool
    {
        if(is_null($algorithm)){
            $algorithm = self::ALGORITHM;
        }

        if(is_null($secret)){
            $secret = self::SECRET;
        }

        list($headerEncoded, $payloadEncoded, $signatureEncoded) = explode('.', $jwt);

        $dataEncoded = "$headerEncoded.$payloadEncoded";

        $signature = $this->base64UrlDecode($signatureEncoded);

        $rawSignature = hash_hmac($algorithm, $dataEncoded, $secret, true);

        return hash_equals($rawSignature, $signature);
    }
}