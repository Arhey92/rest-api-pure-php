<?php
/**
 * Created by PhpStorm.
 * User: Arhey
 * Date: 16.04.2019
 * Time: 3:31
 */

/**
 * @param $method
 * @return array
 */
function getFormData($method) {
    $data = array();
    $url = (isset($_GET['q'])) ? $_GET['q'] : '';
    $url = rtrim($url, '/');
    $urls = explode('/', $url);

    //remove 'api' from url
    unset($urls[0]);
    $data['class'] = $urls[1];
    if(isset($urls[2])){
        $data['id'] = $urls[2];
    }
    if(isset($urls[3])){
        $data['child_class'] = $urls[3];
    }

    if(isset($urls[4])){
        $data['task_id'] = $urls[4];
    }

    if ($method === 'GET'){
        return $data;
    }
    if ($method === 'POST'|| $method === 'PUT' || $method === 'PATCH' || $method === 'DELETE'){
        $data['body'] = json_decode(file_get_contents('php://input'), true);

        return $data;
    }

    header('HTTP/1.0 405 Method Not Allowed');
    echo json_encode(array(
        'error' => 'Method Not Allowed'
    ));
    die();
}