<?php

namespace app\controllers;

require_once 'app/models/User.php';
require_once 'app/helpers/get_bearer_token.php';

use app\models\User;

class UserController
{
    private $model;

    /**
     * UserController constructor.
     * @param $method
     * @param $data
     */
    public function __construct($method, $data)
    {
        $jwt = getBearerToken();

        $user = new User();
        $isCorrect = $user->verifyJWT($jwt);

        if(!$isCorrect){
            echo json_encode(array('status' => 400));
            die();
        }

        $this->model = new User();

        switch ($method) {
            case 'GET':
                $this->show($data);
                break;
            case 'POST':
                $this->store($data['body']);
                break;
            case 'PUT':
                $this->update($data['body'], $data['id']);
                break;
            case 'DELETE':
                $this->destroy($data['id']);
                break;
            default:
                //When the method is different of the previous methods, return an error message.
                echo json_encode(array('status' => 405));
                break;
        }
    }

    /**
     * @param $data
     */
    public function login($data)
    {
        $result = $this->model->isUserDataCorrect($data['email'], $data['password']);

        if(!$result){
            $this->error();
        }else{
            header('HTTP/1.0 200 OK');
            echo json_encode($result);
        }

        return;
    }

    /**
     * @param $data
     */
    public function show($data)
    {
        $result = $this->model->getById($data['id']);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 200 OK');
            echo json_encode($result);
        }

        return;
    }

    /**
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        //todo check if user already exist

        $result = $this->model->update($data, $id);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 200 OK');
            echo json_encode($result);
        }

        return;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id)
    {
        $result = $this->model->destroy($id);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 204 No Content');
            echo json_encode($result);
        }

        return;
    }

    public function error()
    {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(array(
            'error' => 'Bad Request'
        ));

        return;
    }
}