<?php

namespace app\controllers;

require_once 'app/models/Task.php';
require_once 'app/models/User.php';
require_once 'app/helpers/get_bearer_token.php';

use app\models\Task;
use app\models\User;

class TaskController
{
    private $model;

    /**
     * UserController constructor.
     * @param $method
     * @param $data
     */
    public function __construct($method, $data)
    {
        $jwt = getBearerToken();

        $user = new User();
        $isCorrect = $user->verifyJWT($jwt);

        if(!$isCorrect){
            echo json_encode(array('status' => 400));
            die();
        }

        $this->model = new Task();

        switch ($method) {
            case 'GET':
                if(isset($data['task_id'])){
                    $this->setCompleted($data['task_id']);
                }else{
                    $this->showList($data['id']);
                }
                break;
            case 'POST':
                $this->store($data['id'], $data['body']);
                break;
            case 'DELETE':
                $this->destroy($data['id']);
                break;
            default:
                //When the method is different of the previous methods, return an error message.
                echo json_encode(array('status' => 405));
                break;
        }
    }

    /**
     * @param $userId
     */
    public function showList($userId)
    {
        $sort = 'ASC';
        if(isset($_GET['sort'])){
            $sort = $_GET['sort'];
        }

        $limit = 2;
        if(isset($_GET['limit'])){
            $limit = $_GET['limit'];
        }

        $page = 1;
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }

        $result = $this->model->getList($userId, $sort, $limit, $page);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 200 OK');
            echo json_encode($result);
        }

        return;
    }

    /**
     * @param $userId
     * @param $data
     */
    public function store($userId, $data)
    {
        if(!isset($data['title']) || !isset($data['due_date']) || !isset($data['priority']) || !isset($userId)){
            $this->error();
            die();
        }

        $result = $this->model->create($userId, $data);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 201 Created');
            echo json_encode($result);
        }

        return;
    }

    /**
     * @param int $taskId
     */
    public function setCompleted(int $taskId)
    {
        $result = $this->model->completed($taskId);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 200 OK');
            echo json_encode($result);
        }

        return;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id)
    {
        $result = $this->model->destroy($id);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 204 No Content');
            echo json_encode($result);
        }

        return;
    }

    public function error()
    {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(array(
            'error' => 'Bad Request'
        ));

        return;
    }
}