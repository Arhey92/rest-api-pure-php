<?php

namespace app\controllers;

require_once 'app/models/User.php';

use app\models\User;

class RegisterController
{
    private $model;

    /**
     * RegisterController constructor.
     * @param $method
     * @param $data
     */
    public function __construct($method, $data)
    {
        $this->model = new User();

        switch ($method) {
            case 'POST':
                $this->store($data['body']);
                break;
            default:
                //When the method is different of the previous methods, return an error message.
                echo json_encode(array('status' => 405));
                break;
        }
    }

    /**
     * @param $data
     */
    public function store($data)
    {
        if(!isset($data['email']) || !isset($data['password'])){
            $this->error();
            return;
        }

        $result = $this->model->create($data);

        if(empty($result)){
            $this->error();
        }else{
            header('HTTP/1.0 201 Created');
            echo json_encode($result);
        }

        return;
    }

    public function error()
    {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(array(
            'error' => 'Bad Request'
        ));

        return;
    }
}