<?php

namespace app\controllers;

require_once 'app/models/User.php';

use app\models\User;

class LoginController
{
    private $model;

    /**
     * LoginController constructor.
     * @param $method
     * @param $data
     */
    public function __construct($method, $data)
    {
        $this->model = new User();

        switch ($method) {
            case 'POST':
                $this->login($data['body']);
                break;
            default:
                //When the method is different of the previous methods, return an error message.
                echo json_encode(array('status' => 405));
                break;
        }
    }

    /**
     * @param $data
     */
    public function login($data)
    {
        $result = $this->model->isUserDataCorrect($data['email'], $data['password']);

        if(!$result){
            $this->error();
        }else{
            header('HTTP/1.0 200 OK');
            echo json_encode($result);
        }

        return;
    }

    public function error()
    {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(array(
            'error' => 'Bad Request'
        ));

        return;
    }
}