## REST API (only php):
```text
1. Pull from repository
2. Check all requests by postman
```

- User
    - Register
    - Login
    - Get user
    - Delete user
- Tasks
    - Get all user tasks
    - Create new task
    - Delete task
    - Mark is done
    - Get all user tasks with sorting and pagination
    
    
## User
### Register
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/api/register
Headers: {
    Content-Type: application/json
}
```

Тело запроса:
```json
{
	"email": "arthas@gmail.com",
	"password": 123123
}
```

Ответ сервера должен быть следующим(`Status: 201 Created`):
```json
{
    "status": 201,
    "token": "eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0"
}
```

### Login
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/api/login
Headers: {
    Content-Type: application/json
}
```

Тело запроса:
```json
{
	"email": "arthas@gmail.com",
	"password": 123123
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "status": 200,
    "token": "eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0"
}
```

### Get user
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/api/user/{user_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "id": "1",
    "email": "arthas@gmail.com",
    "password": "$2y$10$/4kUkUssHJGYYVVUO4UNVe4AKsPO77c/vxHsekBCC0F8ltWwK4vmq"
}
```
### Delete user
Параметры запроса:
```text
Method: DELETE
URL: http://{server-site-url}/api/user/{user_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 204 No Content`):
```json
[]
```

### Get all user tasks (with not required options)
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/api/user/{user_id}/task
Headers: {
    Content-Type: application/json,
    Authorization: Bearer eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "id": "1",
    "email": "arthas@gmail.com",
    "password": "$2y$10$/4kUkUssHJGYYVVUO4UNVe4AKsPO77c/vxHsekBCC0F8ltWwK4vmq"
}
```

### Create new task
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/api/user/{user_id}/task
Headers: {
    Content-Type: application/json
    Authorization: Bearer eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0
}
```

Тело запроса:
```json
{
	"title": "buy milk",
	"due_date": "2019-04-20 15:15",
	"priority": "Hight"
}
```

Ответ сервера должен быть следующим(`Status: 201 Created`):
```json
{
    "status": 201
}
```

### Delete task
Параметры запроса:
```text
Method: DELETE
URL: http://{server-site-url}/api/user/{user_id}/task/{task_id}
Headers: {
    Content-Type: application/json
    Authorization: Bearer eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0
}
```

Ответ сервера должен быть следующим(`Status: 204 No Content`):
```json
[]
```

### Mark is done
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/api/user/{user_id}/task/{task_id}?status=1
Headers: {
    Content-Type: application/json
    Authorization: Bearer eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "status": 200
}
```

### Get all user tasks with sorting and pagination
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/api/user/{user_id}/task?sort=[asc/desc]&limit=3&page=2
Headers: {
    Content-Type: application/json,
    Authorization: Bearer eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJ1c2VyX2lkIjoiMSIsImVtYWlsIjoiYXJ0aGFzQGdtYWlsLmNvbSJ9.M4mryJCsTPqiHdetBxibXeLae2ozQw0FpsZ3-lTH5J0
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "id": "1",
    "email": "arthas@gmail.com",
    "password": "$2y$10$/4kUkUssHJGYYVVUO4UNVe4AKsPO77c/vxHsekBCC0F8ltWwK4vmq"
}
```
